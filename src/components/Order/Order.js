import React from 'react';
import OrderItem from "./OrderItem/OrderItem";

const Order = props => {
  let count = 0;
  return (

      <div className="shop__order">
        <h2 className="order__heading">Order Details</h2>
        <div className="order__info">
          {props.orders.map((order) => {
            count++;
            return (
                <OrderItem
                    key={count}
                    name={order.name}
                    count={order.count}
                    price={order.price}
                    remove={() => props.remove(order.name)}
                />
            )
          })}
        </div>
        <div className="order__total">{props.total !== 0 ? `Total: ${props.total}` : ''}</div>
      </div>
  );
};

export default Order;
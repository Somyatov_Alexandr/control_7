import React from 'react';

const OrderItem = props => {
  return (
      <div className="order__item">
        <div className="order__name">
          {props.name}
        </div>
        <div className="order__calc">
          <div className="order__count">x{props.count}</div>
          <div className="order__sum">
            {props.price} KGS
          </div>
          <button className="order__remove" onClick={props.remove}>X</button>
        </div>
      </div>
  );
};

export default OrderItem;
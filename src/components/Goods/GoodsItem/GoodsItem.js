import React from 'react';

const GoodsItem = props => {
  return (
      <div className="shop__add-item" onClick={props.addOrder}>
        <div className="add-item__icon">
          <img src={`img/${props.type}.png`} alt={`${props.type}`} />
          {/*img here*/}
        </div>
        <div className="add-item__info">
          <h3 className="add-item__title">
            {props.type}
          </h3>
          <div className="add-item__price">
            Price: <span>{props.price}</span> KGS
          </div>
        </div>
      </div>
  );
};

export default GoodsItem;
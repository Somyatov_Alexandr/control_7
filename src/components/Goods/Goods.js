import React from 'react';
import GoodsItem from "./GoodsItem/GoodsItem";

const Goods = props => {
  let count = 0;
  return (
      <div className="shop__add">
        <h2 className="add__heading">
          Add items
        </h2>
        <div className="shop__add-list">
          {props.goods.map((good) => {
            count++;
            return (
                <GoodsItem key={count} type={good.name} price={good.price} addOrder={() => props.addOrder(good.name, good.price)}/>
            )
          })}
        </div>
      </div>
  );
};

export default Goods;
import React, { Component } from 'react';
import './App.css';
import Goods from "../../components/Goods/Goods";
import Order from "../../components/Order/Order";

class App extends Component {
  state = {
    orders: [],
    total: 0
  };

  goods = [
    {name: 'hamburger', price: 95},
    {name: 'coffee', price: 50},
    {name: 'cookies', price: 60},
    {name: 'cake', price: 75},
    {name: 'fries', price: 40},
    {name: 'hotDog', price: 70},
    {name: 'chocolate', price: 90},
    {name: 'kebab', price: 120}
  ];

  addOrder = (name, price) => {

    let ordersCopy = [...this.state.orders];
    let total = 0;

    const index = ordersCopy.findIndex(item => item.name === name);
    if (index === -1) {
      let obj = {name: name, price: price, count: 1};

      ordersCopy.push(obj);

    } else {
      let itemCopy = {...ordersCopy[index]};
      itemCopy.count++;
      itemCopy.price += price;
      ordersCopy[index] = itemCopy;
    }

    ordersCopy.forEach((order) => {
      total += order.price;
    });

    this.setState({orders: ordersCopy, total: total});
  };

  removeOrderItem = (name) => {

    let ordersCopy = [...this.state.orders];
    let total = 0;
    const index = ordersCopy.findIndex(item => item.name === name);
    if (index !== -1) {
      ordersCopy.splice(index, 1);
    }


    ordersCopy.forEach((order) => {
      total += order.price;
    });

    this.setState({orders: ordersCopy, total: total});
  };


  render() {
    return (
      <div className="App shop">
        <Order orders={this.state.orders} total={this.state.total} remove={this.removeOrderItem}/>
        <Goods goods={this.goods} addOrder={this.addOrder} orders={this.state.orders} />
      </div>
    );
  }
}

export default App;
